
 # RPICO32 – RP2040 / IEEE802.11 2.4GHz module

The RPICO32 is an Arduino/Micropython compatible embedded micro controller module based on the Raspberry Pico chip RP2040 paired with an ESP8285 WiFi subsystem. The RPICO32 combines the raw power of the RP2040 with the simple to use Espressif ESP8285 WiFi creating an extremely easy to use module that can be integrated into your products.

The RPICO32 module is based on the design of our existing Challenger RP2040 WiFi board meaning that existing software can be ported without any major effort and new board support packages for Arduino and CircuitPython simplfies the mapping of any hardware differences. RP2040 micro controller

RP2040 is the debut microcontroller from Raspberry Pi. With a large on-chip SRAM memory, symmetric dual-core processor complex, deterministic bus fabric, and rich peripheral set augmented with our unique Programmable I/O (PIO) subsystem, The RP2040 provides professional users with unrivalled power and flexibility. With detailed documentation, a polished MicroPython port, and a UF2 bootloader in ROM, it has the lowest possible barrier to entry for beginner and hobbyist users.

RP2040 is a stateless device, with support for cached execute-in-place from external QSPI memory. This design decision allows you to choose the appropriate density of non-volatile storage for your application, and to benefit from the low pricing of commodity Flash parts. The RP2040 is manufactured on a modern 40 nm process node, delivering high performance, low dynamic power consumption, and low leakage, with a variety of low-power modes to support extended-duration operation on battery power. Whatever your microcontroller application — from machine learning to motor control, from agriculture to audio — RP2040 has the performance, feature set, and support to make your product fly.

The RPICO32 module also contain a 12MHz crystal that drives the RP2040 to as well as a 8Mbyte 133MHz QSPI FLASH memory well suited for full speed operation of the RP2040.

## WiFi

The RPICO32 module is based on the ESP8285 WiFi chip. For those of you that is unfamiliar with this device, it is basically an ESP8266 device with an integrated 1MByte of flash memory. This allows us to have an AT command interpreter inside this chip that the main controller can talk to and connect to you local WiFi network. The communications channel between the two devices is one of the UARTs on the main controller and the standard UART on the ESP8285. As simple as it can be.

As mentioned the ESP8285 chip comes pre-flashed with Espressif’s AT command interpreter stored in the internal 1MByte of the ESP8285. This interpreter support most of the operating and sleep modes of the standard ESP8266 framework which makes it easy to work with. Talking to the device is as easy as opening the primary serial port (Serial1, UART0), resetting the ESP8285 and start listening for events and sending commands. Check out the examples for more details on how to do this.,

## Form factor
![RPICO32 form factor](https://ilabs.se/wp-content/uploads/2022/01/mech-768x497.png "Test")
